//
// Created by jeroen on 6/28/15.
//

#ifndef HANGMAN_UTILS_H
#define HANGMAN_UTILS_H

#include <iostream>
#include <vector>
#include <sstream>

void clearScreen();
std::string vecToString(std::vector<char> input);  // Constructs a string out of a vector

#endif //HANGMAN_UTILS_H

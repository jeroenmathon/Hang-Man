//
// Created by jeroen on 6/28/15.
//

#ifndef HANGMAN_FILEIO_H
#define HANGMAN_FILEIO_H

#include <fstream>
#include <iostream>

class FIO
{
public:
    // Constructors and Destructors
    FIO(std::string file);
    ~FIO();

    // Public routines
    int getMaxLines();                      // Returns the amount of lines in a file
    std::string getLine(int lineNumber);    // Extracts a line from the file

private:
    std::ifstream file;     // File to be opened
    std::string filePath;   // The path for the file
};

#endif //HANGMAN_FILEIO_H

//
// Created by jeroen on 6/28/15.
//

#include "fileio.h"

FIO::FIO(std::string file)
{
    filePath = file;

}

FIO::~FIO()
{

}

int FIO::getMaxLines()
{
    file.open(filePath);
    if(file.is_open())
    {
        std::string temp;
        int lineMax(0);

        while (std::getline(file, temp)) lineMax++;
        file.close();

        return lineMax;
    }

    return 0;
}

std::string FIO::getLine(int lineNumber)
{
    file.open(filePath);
    if(file.is_open())
    {
        std::string line;

        char tmp;
        for(int i(0); i < lineNumber; i++)
        {
            while(file.get(tmp))
            {
                if(tmp == '\n') break;
            }
        }

        std::getline(file, line);
        file.close();

        return line;
    }

    return "[ERROR]";
}
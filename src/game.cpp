//
// Created by jeroen on 6/28/15.
//
#include "game.h"

Game::Game()
{

}

Game::~Game()
{

}

bool Game::verify()
{
    contains = false;
    for(int i(0); i < currentWord.length(); i++)
    {
        if(tolower(playerGuess.at(0)) == tolower(currentWord.at(i)))
        {
            guess.at(i) = tolower(playerGuess.at(0));
            contains = true;
        }
    }

    return contains;
}

void Game::startNewGame()
{
    /* Random number generation and word preparation */
    srand(time(NULL));
    currentWord = fileio->getLine(rand() % fileio->getMaxLines()); // Get a random line from the dictionary

    for(int i(0);i < currentWord.length(); i++) guess.push_back('_'); // Prepare the guess array

    /* Main Game loop */
    while(!exitFlag) {
        clearScreen();
        printHangman(mistakes);
        std::cout << "Used letters:";

        /* Print out all used characters */
        for (auto c : usedChars)
        {
            std::cout << c;
        }

        /* Print out the guess text */
        std::cout << "\nWord:" << vecToString(guess) << "(" << currentWord.length() << ")\n";
        std::cout << "Guess:";

        /* Get player input and verify the users input */
        std::cin >> playerGuess;

        if (!verify()) {
            mistakes++;
            bool isInUsed = false;

            for (auto c: usedChars) {
                if (playerGuess.at(0) == c) isInUsed = true;
            }
            if (isInUsed) usedChars.push_back(tolower(playerGuess.at(0)));
        }

        playerGuess.clear();


        /* Check for completion */
        complete = true;
        for (int i(0); i < guess.size(); i++)
        {
            if (guess.at(i) == '_') complete = false;
        }

        if (mistakes >= 11) // The player failed the game
        {

            clearScreen();
            printHangman(mistakes);
            std::cout << "Aw no you lost.\nThe correct word was:" << currentWord << "\n";
            exitFlag = true;
        }

        if (complete)   // The player won the game
        {
            clearScreen();
            printHangman(mistakes);
            std::cout << "Congrats you have guessed the word!\nIt was:" << currentWord << "\n";
            exitFlag = true;
        }
    }
}

void Game::printHangman(int mistakes)
{
    switch(mistakes)
    {
        case 1:
            std::cout << "_________\n";
            break;

        case 2:
            std::cout << "|\n|\n|\n|\n|\n|_________\n";
            break;

        case 3:
            std::cout << " ____\n|\n|\n|\n|\n|\n|_________\n";
            break;

        case 4:
            std::cout << " ____\n|    |\n|\n|\n|\n|\n|_________\n";
            break;

        case 5:
            std::cout << " ____\n|    |\n|    O\n|\n|\n|\n|_________\n";
            break;

        case 6:
            std::cout << " ____\n|    |\n|    O\n|    |\n|\n|\n|_________\n";
            break;

        case 7:
            std::cout << " ____\n|    |\n|    O\n|   /|\n|\n|\n|_________\n";
            break;

        case 8:
            std::cout << " ____\n|    |\n|    O\n|   /|\\\n|\n|\n|_________\n";
            break;

        case 9:
            std::cout << " ____\n|    |\n|    O\n|   /|\\\n|    |\n|\n|_________\n";
            break;

        case 10:
            std::cout << " ____\n|    |\n|    O\n|   /|\\\n|    |\n|   /\n|_________\n";
            break;

        case 11:
            std::cout << " ____\n|    |\n|    O\n|   /|\\\n|    |\n|   / \\\n|_________\n";
            break;

        default:
            break;
    }
}
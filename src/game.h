//
// Created by jeroen on 6/28/15.
//

#ifndef HANGMAN_GAME_H
#define HANGMAN_GAME_H

#include <iostream>
#include <string.h>
#include <vector>
#include "fileio.h"
#include "utils.h"

/// \brief The class that handles the game.

class Game
{
public:
    // Constructors and Destructors
    Game();
    ~Game();

    // Public routines
    void startNewGame();    //!< Starts a new game

private:
    // Private variables
    FIO *fileio = new FIO("./res/dictionary");                     // File controller
    int mistakes = 0;               // Mistakes made by player
    bool exitFlag = false;          // Game loop flag
    bool complete = false;          // Did the game complete the game
    bool contains = false;          // Does the word contain the character
    std::vector<char> guess;        // The guess vector which contains the the word in lines
    std::vector<char> usedChars;    // Characters guessed by the player
    std::string currentWord;        // The current word
    std::string playerGuess;        // Player inputted guess character

    // Private routines
    void printHangman(int mistakes);                // Prints hangman with its mistakes
    bool verify();                                  // Checks whether the players character is in the word
};

#endif //HANGMAN_GAME_H

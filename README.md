# Hang-Man
A simple game of hangman with a custom dictionary.
You can add your own words in the dictionary file located in res/dictionary.
Simply edit it, make a build directory, enter that and type the following command ```cmake .. ; make```.
After that run the program by typing ./hangman and enjoy the game ^_^.